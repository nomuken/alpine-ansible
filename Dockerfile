FROM alpine:latest
LABEL maintainer "Nomuken <nomuken@spica.bz>"
RUN apk update && \
      apk upgrade && \
      apk add ansible openssh
